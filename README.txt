README for cups-x2go
--------------------

The minimal X2Go Server + CUPS setup is: install x2goserver, x2goserver-printing, cups, cups-x2go
on the same machine.

  $ sudo aptitude install x2goserver x2goserver-printing cups cups-x2go

Printing works nearly out of the box, you only have to add a virtual CUPS
queue based on the Virtual X2Go Printer (cups-x2go) backend.

For further details, please visit these X2Go Wiki pages:
http://wiki.x2go.org/doku.php/doc:installation:printing
http://wiki.x2go.org/doku.php/wiki:advanced:multi-node:x2goserver-printing


light+love
Mike Gabriel, 20130917